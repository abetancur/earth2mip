# SPDX-FileCopyrightText: Copyright (c) 2023 NVIDIA CORPORATION & AFFILIATES.
# SPDX-FileCopyrightText: All rights reserved.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import logging
import pathlib
import tempfile
import shutil
import numpy as np
import pandas as pd
import datetime
import earth2mip.networks.fcn as fcn
from earth2mip import registry, inference_ensemble
from earth2mip.initial_conditions import cds
from modulus.distributed import DistributedManager
from os.path import dirname, abspath, join

from google.cloud import storage

def upload_to_gcs(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    # Initialize a storage client.
    storage_client = storage.Client()

    # Get the bucket object for our bucket.
    bucket = storage_client.bucket(bucket_name)

    # Create a new blob (object) in the bucket.
    blob = bucket.blob(destination_blob_name)

    # Upload the local file to GCS.
    blob.upload_from_filename(source_file_name)


logging.basicConfig(level=logging.INFO)

gcs_bucket = "aes-analytics-0002-curated-ibu"
gcs_base_path = pathlib.Path("DataDrivenWeatherOutputs/raw/basic_inference")

model_str = "fcn"
initial_condition_source = "cds"  # cds, gfs
simulation_length = 5*4

prefix = f"{model_str}-{initial_condition_source}-{simulation_length:05}"

# output = "path/"
# start_time = datetime.datetime(2020, 1, 1, 0)

dates = pd.date_range("2022-01-01 00:00:00", "2023-10-31 00:00:00", freq="1D")
# dates = pd.date_range("2022-01-01 00:00:00", "2022-01-03 00:00:00", freq="1D")

for start_time in dates:

    device = DistributedManager().device
    logging.info(f"Loading FCN model onto {device}, this can take a bit")
    package = registry.get_model("e2mip://fcn")
    sfno_inference_model = fcn.load(package, device=device)

    data_source = cds.DataSource(sfno_inference_model.in_channel_names)

    start_time_log = datetime.datetime.now()

    logging.info(f"Running FCN small model for {start_time}")
    ds_raw = inference_ensemble.run_basic_inference(
        sfno_inference_model,
        n=simulation_length,
        data_source=data_source,
        time=start_time,
    )
    logging.info(f"Finish FCN small model for {start_time}")

    ds_raw = ds_raw.to_dataset(dim='channel')
    ds_raw = ds_raw.sel(history=0)

    tmp_path = pathlib.Path(tempfile.mkdtemp())
    print(tmp_path)

    for variable in list(ds_raw.keys()):
        ds = ds_raw[variable]

        file_path = tmp_path.joinpath(f"{variable}.nc4")
        ds.to_netcdf(file_path)

        gcs_path = gcs_base_path.joinpath(
            model_str, 
            initial_condition_source, 
            prefix,
            start_time.strftime("%Y-%m-%d-%H"),
            file_path.name
            )

        upload_to_gcs(
            bucket_name=gcs_bucket, 
            source_file_name=file_path, 
            destination_blob_name=gcs_path.as_posix(),
            )

        logging.info(f"File {file_path} uploaded to gs://{gcs_bucket}/{gcs_path.as_posix()}")

    shutil.rmtree(tmp_path)

    del(device)
    del(package)
    del(sfno_inference_model)
    del(data_source)
    del(ds_raw)
    del(ds)

    end_time_log = datetime.datetime.now()
    duration = end_time_log - start_time_log
    duration_in_minutes = duration.total_seconds() / 60
    logging.info(f"The loop for {start_time} took {duration_in_minutes:.2f} minutes to complete.")

    # break
