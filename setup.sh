#!/bin/bash

set -x

# Initialize conda for the current shell session
source /opt/conda/etc/profile.d/conda.sh

# Initialize conda for future shell sessions
conda init

# Update conda to the latest version
# conda update -y conda

# # Execute a new instance of the bash shell
# exec bash

source /opt/conda/etc/profile.d/conda.sh

conda create -y --name earth2mip python=3.10
conda activate earth2mip

source /opt/conda/etc/profile.d/conda.sh

# Clone the earth2mip repository
cd /mnt/code/

pip install -e .

pip install tensorly tensorly-torch

cd $HOME && git clone https://github.com/NVIDIA/apex
cd $HOME/apex && pip install -v --disable-pip-version-check --no-cache-dir --no-build-isolation --global-option="--cpp_ext" --global-option="--cuda_ext" .

conda install -y -c dglteam dgl

pip install cchardet
pip install --upgrade charset_normalizer
pip install --upgrade aiohttp

pip uninstall -y nvidia-modulus
pip install --upgrade pip
pip install https://github.com/nbren12/modulus/archive/refs/heads/tisr.tar.gz

pip install ruamel.yaml

cat <<EOL >> $HOME/.cdsapirc
url:https://cds.climate.copernicus.eu/api/v2
key:25506:92f02d0e-eee3-40aa-a3b7-63382d81e279
EOL

conda install -y -c conda-forge google-cloud-storage

conda install -y -c conda-forge matplotlib cartopy

conda install -y -c conda-forge nvtop
sudo apt-get -y install htop
sudo apt -y install screen

conda install -y -c conda-forge gcsfs


cd /mnt/code/earth2mip
