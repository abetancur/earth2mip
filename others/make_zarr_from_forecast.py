# %%
from pathlib import Path
import xarray as xr
import dask
from dask.diagnostics import ProgressBar
import gcsfs
import shutil
from google.cloud import storage

# %%
# Parameters
local_base_path = Path('/home/ubuntu/aerth2mip/raw/basic_inference/fcn/cds/fcn-cds-00020/')

# %% [markdown]
# # Functions

# %%
def download_files(bucket_name, gcs_prefix, local_base_path, file_pattern, overwrite=False):
    # Initialize the GCS client
    client = storage.Client()

    # Create the storage client
    bucket = client.get_bucket(bucket_name)

    # List the blobs in the bucket (files are represented as blob objects in GCS)
    blobs = bucket.list_blobs(prefix=gcs_prefix)  # List all objects that start with the prefix

    # Download the files
    for blob in blobs:
        # Only download files matching the pattern
        if blob.name.endswith(file_pattern):
            # Construct the full local path
            local_path = Path(local_base_path) / Path(blob.name[len(gcs_prefix):])
            
            # Skip download if file exists and overwrite is False
            if local_path.is_file() and not overwrite:
                print(f'File {local_path} already exists and will not be overwritten.')
                continue
            
            # Ensure the local directory structure is in place
            local_path.parent.mkdir(parents=True, exist_ok=True)

            # Download the blob to the local path
            blob.download_to_filename(local_path)
            # print(f'Downloaded {blob.name} to {local_path}')

    print('File download process completed.')

# %%
# Function to list all downloaded .nc4 files using pathlib
def list_downloaded_files(local_base_path, file_pattern):
    return list(local_base_path.rglob(file_pattern))  # This will recursively list all files matching the pattern

# Preprocess function
def preprocess(ds):
    # Save the original 'time' data
    original_time_values = ds['time'].values
    
    # Rename 'time' dimension to 'leadtime' and replace the index
    ds = ds.rename({'time': 'leadtime'})
    ds['leadtime'] = range(len(ds.leadtime))
    
    # Create a new 'time' dimension and assign the first value of the original 'time' data
    first_time_value = original_time_values[0]
    ds = ds.expand_dims('time')
    ds = ds.assign_coords(time=[first_time_value])
    
    return ds



# %% [markdown]
# # Download data and make zarr

# %%
file_patterns = [
    # surface
    'msl.nc4',
    'sp.nc4',
    't2m.nc4',
    'u100m.nc4',
    # 'u10m.nc4',
    'v100m.nc4',
    # 'v10m.nc4',

    # integrated
    # 'tcwv.nc4',

    # pressure levels

    'z50.nc4',
    'z250.nc4',
    # 'z500.nc4',
    # 'z850.nc4',
    'z1000.nc4',

    # # 'r500.nc4',
    # # 'r850.nc4',

    't250.nc4',
    # # 't500.nc4',
    # # 't850.nc4',

    'u250.nc4',
    # 'u500.nc4',
    # # 'u850.nc4',
    # 'u1000.nc4',

    # 'v250.nc4',
    # 'v500.nc4',
    # # 'v850.nc4',
    # 'v1000.nc4',
    ]

# %%
for file_pattern in file_patterns:

    print(f"********************** {file_pattern} **********************")

    bucket_name = 'aes-analytics-0002-curated-ibu'
    gcs_prefix = 'DataDrivenWeatherOutputs/raw/basic_inference/fcn/cds/fcn-cds-00020/'
    local_base_path = '/home/ubuntu/aerth2mip/raw/basic_inference/fcn/cds/fcn-cds-00020/'
    overwrite = False  # Change to True if you want to overwrite existing files

    download_files(bucket_name, gcs_prefix, local_base_path, file_pattern, overwrite)


    # **********************************************************************************************

    # Configure Dask to use a progress bar for all operations
    dask.config.set(scheduler='threads')  # Use threaded scheduler; you can adjust as needed
    ProgressBar().register()

    # Use pathlib's glob to list the downloaded files
    downloaded_files = list_downloaded_files(local_base_path, file_pattern)

    # Verify if we have downloaded files
    if not downloaded_files:
        print("No downloaded files found. Please check the download steps.")
    else:
        # Use xarray to open the dataset with preprocessing
        ds = xr.open_mfdataset(
            sorted(downloaded_files),
            preprocess=preprocess,
            concat_dim='time',
            combine='nested',
            parallel=True,
            chunks={'lat': 120, 'lon': 120, 'leadtime': -1}  # Replace with your dataset's dims
        ) 
        
    ds = ds.chunk({'time': 90})

    print(ds)

    # Set the GCS path for the base of the Zarr store
    bucket_name = 'aes-analytics-0002-curated-ibu'
    gcs_prefix = 'DataDrivenWeatherOutputs/processed/basic_inference/fcn/cds/fcn-cds-00020/'
    base_gcs_path = f'gcs://{bucket_name}/{gcs_prefix}'


    fs = gcsfs.GCSFileSystem()

    # Create a unique Zarr store name based on the NetCDF file name pattern, without the .nc4 extension
    # Assuming all files follow a similar naming pattern and taking the name from the first file
    zarr_store_name = os.path.splitext(os.path.basename(downloaded_files[0]))[0]
    gcs_zarr_path = os.path.join(base_gcs_path, f"{zarr_store_name}.zarr")

    print(gcs_zarr_path)

    # Write to a Zarr store on GCS
    # The consolidated=True option can improve performance for reading metadata later
    with ProgressBar():
        ds.to_zarr(store=gcs_zarr_path, mode='w', consolidated=True)

    shutil.rmtree(local_base_path)

    # break

# %%



